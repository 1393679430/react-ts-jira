function debounce(fn, delay = 500) {
  let timer;
  return function () {
    let ctx = this;
    let args = arguments;
    timer && clearTimeout(timer);

    timer = setTimeout(() => {
      fn.apply(ctx, args);
    }, delay);
  };
}

export { debounce };
