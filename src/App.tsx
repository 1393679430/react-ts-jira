import React from "react";
import "./App.css";
import { TestHooks, Login } from "./pages";

function App() {
  return (
    <div className="App">
      <TestHooks />
      <Login />
    </div>
  );
}

export default App;
