import React, { FormEvent } from "react";
import qs from "qs";
import axios from "axios";

const api = process.env.REACT_APP_API_URI;

const Login = () => {
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const elements = event.currentTarget.elements;
    const username = (elements[0] as HTMLInputElement).value;
    const password = (elements[1] as HTMLInputElement).value;
    const params = qs.stringify({ username, password });

    axios({
      method: "POST",
      url: `${api}/login`,
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        username,
        password,
      },
    }).then((response) => {
      console.log(response, "response---");
    });
  };

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="username">username:</label>
        <input type="text" id="username" />
      </div>
      <div>
        <label htmlFor="password">password:</label>
        <input type="password" id="password" autoComplete="off" />
      </div>
      <div>
        <button type="submit">登录</button>
      </div>
    </form>
  );
};

export default Login;
