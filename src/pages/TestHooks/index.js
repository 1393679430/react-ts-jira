import { useState } from "react";
import useDebounce from "../../hooks/useDebounc";

const TestHooks = () => {
  const [count, setCount] = useState(0);
  const debounceCount = useDebounce(count, 500);
  const handleTestUseDebounce = () => {
    setCount((c) => {
      c++;
      return c;
    });
  };

  return (
    <>
      <h2>TestHooks</h2>
      <button onClick={handleTestUseDebounce}>
        测试useDebounce hooks
        <br />
        count: {count}
        <br />
        debounceCount: {debounceCount}
      </button>
    </>
  );
};

export default TestHooks;
