module.exports = {
  extends: [],
  plugins: ["@typescript-eslint", "prettier"],
  parser: "@typescript-eslint/parser",
  rules: {
    "prettier/prettier": "error",
    "prefer-const": "error",
  },
};
